package com.client.application;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import id.co.vostra.vanguardapi.VanguardAPI;

public class MainActivity extends AppCompatActivity {

    public Button btn_connect, btn_disconnect, btn_imei;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_connect = findViewById(R.id.btn_connect);
        btn_disconnect = findViewById(R.id.btn_disconnect);
        btn_imei = findViewById(R.id.btn_getimei);

        VanguardAPI vanguardAPI = new VanguardAPI(getApplicationContext());

        btn_connect.setOnClickListener(view -> {
            try {
                vanguardAPI.connect();
            } catch (Exception e) {
                Log.e(VanguardAPI.TAG, "Exception", e);
            }
        });

        btn_disconnect.setOnClickListener(view -> vanguardAPI.disconnect());

        btn_imei.setOnClickListener(view -> {
            String imei = vanguardAPI.getImei();
            Toast.makeText(getApplicationContext(), imei, Toast.LENGTH_SHORT).show();
        });
    }
}